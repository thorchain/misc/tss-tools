module gitlab.com/thorchain/tss/tss-tools

go 1.13

require (
	github.com/binance-chain/tss-lib v1.3.1
	github.com/cosmos/cosmos-sdk v0.37.4
	github.com/deckarep/golang-set v1.7.1 // indirect
	github.com/golang/protobuf v1.3.4 // indirect
	github.com/rs/zerolog v1.18.0
	github.com/sethgrid/pester v0.0.0-20190127155807-68a33a018ad0
	github.com/stretchr/testify v1.5.1 // indirect
	github.com/urfave/cli v1.22.2
	gitlab.com/thorchain/thornode v0.0.0-20200226210501-df779dc7dc70
	gitlab.com/thorchain/tss/go-tss v0.0.0-20200501053210-59e30c277d94
	golang.org/x/net v0.0.0-20200226121028-0de0cce0169b // indirect
	golang.org/x/tools v0.0.0-20200227193342-b3f10971cb29 // indirect
	google.golang.org/genproto v0.0.0-20200227132054-3f1135a288c9 // indirect
)
